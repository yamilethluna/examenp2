import { StatusBar } from 'expo-status-bar';
import { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Button, TextInput, ScrollView } from 'react-native';
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, onAuthStateChanged, signOut } from "firebase/auth";

// Configuracion de Firebase
const firebaseConfig = {
  apiKey: "AIzaSyA5vQ1DxZHHly_7pCI9wVvXXMMhusCFTGc",
  authDomain: "proyecto1-2cee1.firebaseapp.com",
  projectId: "proyecto1-2cee1",
  storageBucket: "proyecto1-2cee1.appspot.com",
  messagingSenderId: "766803786065",
  appId: "1:766803786065:web:7a0687a21c04c144f43b70",
  measurementId: "G-NDEGNPBLVG"
};

// Inicializacion de Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

const SignIn = ({ email, setEmail, password, setPassword, handleSignIn }) => {
  return (
    <View style={styles.authContainer}>
      <Text style={styles.title}>Iniciar Sesión</Text>
      <TextInput
        style={styles.input}
        value={email}
        onChangeText={setEmail}
        placeholder='Correo Electrónico'
        autoCapitalize='none'
      />
      <TextInput
        style={styles.input}
        value={password}
        onChangeText={setPassword}
        placeholder='Contraseña'
        secureTextEntry
      />
      <Button
        title='Iniciar Sesión'
        onPress={handleSignIn}
        color='#27ae60'
      />
    </View>
  );
};

const SignUp = ({ email, setEmail, password, setPassword, handleSignUp }) => {
  return (
    <View style={styles.authContainer}>
      <Text style={styles.title}>Registrarse</Text>
      <TextInput
        style={styles.input}
        value={email}
        onChangeText={setEmail}
        placeholder='Correo Electrónico'
        autoCapitalize='none'
      />
      <TextInput
        style={styles.input}
        value={password}
        onChangeText={setPassword}
        placeholder='Contraseña'
        secureTextEntry
      />
      <Button
        title='Registrarse'
        onPress={handleSignUp}
        color='#27ae60'
      />
    </View>
  );
};

const AuthenticatedScreen = ({ user, handleSignOut }) => {
  return (
    <View style={styles.authContainer}>
      <Text style={styles.title}>Bienvenido</Text>
      <Text style={styles.emailText}>{user.email}</Text>
      <Button title="Cerrar Sesión" onPress={handleSignOut} color="#e74c3c" />
    </View>
  );
};

const App = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [user, setUser] = useState(null);
  const [isLogin, setIsLogin] = useState(true);

  const auth = getAuth(app);
  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      setUser(user);
    });

    return () => unsubscribe();
  }, [auth]);

  const handleSignIn = async () => {
    try {
      await signInWithEmailAndPassword(auth, email, password);
      console.log("Usuario ha iniciado sesión exitosamente!");
    } catch (error) {
      console.error('Error al iniciar sesión:', error.message);
    }
  };

  const handleSignUp = async () => {
    try {
      await createUserWithEmailAndPassword(auth, email, password);
      console.log("Usuario registrado exitosamente!");
    } catch (error) {
      console.error('Error al registrarse:', error.message);
    }
  };

  const handleSignOut = async () => {
    try {
      await signOut(auth);
      console.log("Usuario ha cerrado sesión exitosamente!");
    } catch (error) {
      console.error('Error al cerrar sesión:', error.message);
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      {user ? (
        <AuthenticatedScreen user={user} handleSignOut={handleSignOut} />
      ) : (
        isLogin ? (
          <SignIn
            email={email}
            setEmail={setEmail}
            password={password}
            setPassword={setPassword}
            handleSignIn={handleSignIn}
          />
        ) : (
          <SignUp
            email={email}
            setEmail={setEmail}
            password={password}
            setPassword={setPassword}
            handleSignUp={handleSignUp}
          />
        )
      )}
      <Text
        style={styles.toggleText}
        onPress={() => setIsLogin(!isLogin)}
      >
        {isLogin ? '¿No tienes una cuenta? Regístrate' : '¿Ya tienes una cuenta? Inicia Sesión'}
      </Text>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
    backgroundColor: '#dcd0ff', 
  },
  authContainer: {
    width: '80%',
    maxWidth: 400,
    backgroundColor: '#fff',
    padding: 20,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  title: {
    fontSize: 26,
    marginBottom: 20,
    textAlign: 'center',
    color: '#34495e',
  },
  input: {
    height: 45,
    borderColor: '#bdc3c7',
    borderWidth: 1,
    marginBottom: 20,
    padding: 10,
    borderRadius: 5,
  },
  buttonContainer: {
    marginBottom: 20,
  },
  toggleText: {
    color: '#000', 
    textAlign: 'center',
    marginTop: 30,
    fontSize: 16,
  },
  emailText: {
    fontSize: 20,
    textAlign: 'center',
    marginBottom: 20,
    color: '#2c3e50',
  },
});

export default App;
